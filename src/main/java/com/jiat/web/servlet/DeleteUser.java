package com.jiat.web.servlet;

import com.jiat.web.db.DBConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;

@WebServlet(name = "deleteUser", urlPatterns = "/deleteUser")
public class DeleteUser extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        if (!id.equals("")) {
            Connection connection = null;
            try {
                connection = DBConnection.getConnection();

                boolean isUserFound = connection.createStatement().executeQuery("SELECT * FROM user WHERE id = " + id).next();
                if (isUserFound) {
                    connection.createStatement().execute("DELETE FROM user WHERE id = " + id);
                    resp.sendRedirect("index.jsp");
                } else {
                    resp.getWriter().write("User not found");
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

}
