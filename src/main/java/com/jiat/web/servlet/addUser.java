package com.jiat.web.servlet;

import com.jiat.web.db.DBConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;

@WebServlet(name = "addUser", urlPatterns = "/addUser")
public class addUser extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String mobile = req.getParameter("mobile");
        String nic = req.getParameter("nic");

        if (!name.equals("") && !mobile.equals("") && !nic.equals("")) {
            Connection connection = null;
            try {
                connection = DBConnection.getConnection();

                boolean usernameAlreadyTaken = connection.createStatement().executeQuery("SELECT * FROM user WHERE mobile = '" + mobile + "'").next();
                if (!usernameAlreadyTaken) {
                    connection.createStatement().execute("INSERT INTO user (name, mobile, nic) VALUES ('" + name + "', '" + mobile + "','" + mobile + "')");
                    resp.getWriter().write("User added successfully");
                    resp.sendRedirect("index.jsp");
                } else {
                    resp.getWriter().write("Username already taken");
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            resp.getWriter().write("Please fill all the fields");
        }
    }
}
