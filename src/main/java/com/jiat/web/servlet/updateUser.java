package com.jiat.web.servlet;

import com.jiat.web.db.DBConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;

@WebServlet(name = "updateUser", urlPatterns = "/updateUser")
public class updateUser extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userId = req.getParameter("id");
        String name = req.getParameter("name");
        String mobile = req.getParameter("mobile");
        String nic = req.getParameter("nic");

        if (!userId.equals("") && !name.equals("") && !mobile.equals("") && !nic.equals("")) {
            Connection connection = null;
            try {
                connection = DBConnection.getConnection();
                boolean userHasAnAccount = connection.createStatement().executeQuery("SELECT * FROM user WHERE id = " + userId).next();

                if (userHasAnAccount) {
                    boolean usernameAlreadyTaken = connection.createStatement().executeQuery("SELECT * FROM user WHERE mobile = '" + mobile + "' AND id != " + userId).next();

                    if (!usernameAlreadyTaken) {
                        connection.createStatement().execute("UPDATE user SET name = '" + name + "', mobile = '" + mobile + "', nic = '" + nic + "' WHERE id = " + userId);
                        resp.getWriter().write("User updated successfully");
                        resp.sendRedirect("index.jsp");
                    } else {
                        resp.getWriter().write("New username already taken, please try another one");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            resp.getWriter().write("Please fill all the fields");
        }
    }

}
