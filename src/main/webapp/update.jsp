<%@ page import="java.sql.Connection" %>
<%@ page import="com.jiat.web.db.DBConnection" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %><%--
  Created by IntelliJ IDEA.
  User: aathi
  Date: 4/3/2023
  Time: 11:59 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User Update</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">
</head>

<%
    if (request.getParameter("id") == null || request.getParameter("id").equals("")) {
        response.sendRedirect("index.jsp");
    }

    int id = Integer.parseInt(request.getParameter("id"));

    String name = "";
    String mobile = "";
    String nic = "";

    Connection connection = null;
    try {
        connection = DBConnection.getConnection();
        ResultSet userrs = connection.createStatement().executeQuery("SELECT * FROM user WHERE id = " + id);

        if (userrs.next()) {
            name = userrs.getString("name");
            mobile = userrs.getString("mobile");
            nic = userrs.getString("nic");
        } else {
            response.getWriter().write("User not found");
            return;
        }
    } catch (Exception e) {
        e.printStackTrace();
    } finally {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

%>

<body>
<h4 class="fw-bold mb">User Update</h4>
<form action="updateUser" method="post">
    <h4 class="fw-bold mb-2 text-primary">Update User Details</h4>

    <div class="mb-3">
        <label class="form-label">Id</label>
        <input type="text" name="id" class="form-control" value="<%=id%>"
               style="pointer-events: none;background-color:#E9ECEF">
    </div>

    <div class="mb-2">
        <label class="form-label">Name</label>
        <input type="text" name="name" class="form-control" value="<%=name%>">
    </div>
    <div class="mb-2">
        <label class="form-label">Mobile</label>
        <input type="text" name="mobile" class="form-control" value="<%=mobile%>">
    </div>
    <div class="mb-2">
        <label class="form-label">NIC</label>
        <input type="text" name="nic" class="form-control" value="<%=nic%>">
    </div>
    <div>
        <button type="submit" class="btn btn-dark">Update User</button>
    </div>
</form>
</body>
</html>
