<%--
  Created by IntelliJ IDEA.
  User: aathi
  Date: 4/3/2023
  Time: 10:51 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add User</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">
</head>
<body>
<form action="addUser" method="post" class="">
    <h4 class="fw-bold mb-2">User Registration</h4>

    <div class="mb-2">
        <label class="form-label">Name</label>
        <input type="text" class="col-9" name="name" class="form-control">
    </div>
    <div class="mb-2">
        <label class="form-label">Mobile</label>
        <input type="text" class="col-9" name="mobile" class="form-control">
    </div>
    <div class="mb-2">
        <label class="form-label">NIC</label>
        <input type="text" class="col-9" name="nic" class="form-control">
    </div>
    <div>
        <button type="submit" class="btn btn-primary">Register New User</button>
    </div>
</form>
</body>
</html>
