<%@ page import="java.sql.Connection" %>
<%@ page import="com.jiat.web.db.DBConnection" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %><%--
  Created by IntelliJ IDEA.
  User: aathi
  Date: 4/3/2023
  Time: 11:37 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User Manager</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">
</head>
<body>
<h4 class="fw-bold mb">User Management</h4>
<table class="table">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Mobile</th>
        <th scope="col">NIC</th>
        <th scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>

    <%
        Connection connection = null;
        try {
            connection = DBConnection.getConnection();
            ResultSet userrs = connection.createStatement().executeQuery("SELECT * FROM user");

            while (userrs.next()) {
                out.println("<tr>");
                out.println("<th scope='row'>" + userrs.getString("id") + "</th>");
                out.println("<td>" + userrs.getString("name") + "</td>");
                out.println("<td>" + userrs.getString("mobile") + "</td>");
                out.println("<td>" + userrs.getString("nic") + "</td>");
                out.println("<td>" +
                        "<a href='update.jsp?id=" + userrs.getString("id") + "' class='btn btn-primary'>Edit</a>" +
                        "<a href='deleteUser?id=" + userrs.getString("id") + "' class='btn btn-danger ms-2'>Delete</a>" +
                        "</td>");
                out.println("</tr>");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }

    %>

    </tbody>
</table>
<a href="addUser.jsp" class="btn btn-warning">Add new user</a>
</body>
</html>
